<?php
	require './vendor/autoload.php';
	
	class Conexion{
		private static $conexion;

		public function Conexion(){
			
		}

		public static function crearConexion(){
			$uri="mongodb://localhost:27017";
			try{
                $conexion= new MongoDB\Client($uri);

                if(!isset(self::$conexion)){
                	$db=$conexion->selectDatabase('NoticieroDB');
					return $db;
                }
					
            }catch(PDOException $e){
                print "ERROR".$e->getMessage()."<br>";
                die();
            }
			
		}


	}